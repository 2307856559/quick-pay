define(function(require, exports, module) {
	var urls = require('restMapping');
	module.exports = {
		isMobile: function(){
			var u = navigator.userAgent;
			return !!u.match(/AppleWebKit.*Mobile.*/); 
		},
		isAndroid: function(){
			var u = navigator.userAgent;
			return u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
		},
		isIOS: function(){
			var u = navigator.userAgent;
			return !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
		},
		REST_REQ_URL: window.location.protocol + '//' + window.location.host,
		req: function(key, data, callback, customCb, dataType, async, errorCallBack) {
			var mapping_obj = urls[key];
	        if(!mapping_obj)  return console.log('未设置 rest mapping[ ' + key + ']');
	        
	        var url = mapping_obj.u;
	        if(!url) return console.log('未设置 [' + key + '] url');
	        url = this.REST_REQ_URL + mapping_obj.u
	        
	        console.log(url);
	        var type = mapping_obj.m ? mapping_obj.m : 'get';
	        async = async || true
	        dataType = dataType || 'json'
	        errorCallBack = errorCallBack || this.onresterror
	        data = data || {}
	        
	        var ajaxParams = {
				type: type,
				url: url,
				async: async,
				data: data,
				dataType:dataType,
				success: callback,
				error: function(){
					errorCallBack.call(this,arguments)	
				}.bind(this)
			};
			//自定义钩子函数
			if (customCb) {
				for(var o in customCb) {
					ajaxParams[o] = customCb[o];	
				}
			}
	        $.ajax(ajaxParams);
		},
		onresterror: function(xhr){
	        console.log(xhr[1]);
	        console.log(xhr[2]);
	        if(xhr[0].status === 401) {
	            // loginOut
	        } else if(!navigator.onLine) {
	            alert('网络异常');
	        } else {
	            alert('服务器内部错误，请稍后重试.');
	        }
	    },
	    getUrlParams: function() {
		    var url = location.search;
		    var theRequest = {};
		    if (~url.indexOf("?")) {
		        var str = url.substr(1);
		        var strs = str.split("&");
		        for(var i = 0; i < strs.length; i ++) {
		            theRequest[strs[i].split("=")[0]] = decodeURI(strs[i].split("=")[1]);
		        }
		    }
		    return theRequest;
		},
		redirect: function(url){
			window.location.href = url;
		},
		open:function(url){
			window.open(url);
		} 
	}
});